import 'package:flutter/material.dart';
import 'package:routemaster/routemaster.dart';

import 'farm/dashboard.dart';
import 'farm/products/inventory_list.dart';

class ROUTES {
  static const FarmDashboard = '/';
  static const FarmInventory = '/farm/inventory';
}

final appRoutes = RouteMap(
  routes: {
    ROUTES.FarmDashboard: (_) => MaterialPage(child: DashboardScreen()),
    ROUTES.FarmInventory: (_) => MaterialPage(child: InventoryListScreen()),
    // TODO might be worth wrapping components to make state/persistence layer easily changeable
    // i.e. InventoryListComponent using a Provider to pass state and navigation functions to screens
  },
);
