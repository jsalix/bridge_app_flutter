import 'package:bridge_app/farm/farm.dart';
import 'package:flutter/material.dart';
// import 'package:routemaster/routemaster.dart';

// import 'routes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bridge App',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      // routerDelegate: RoutemasterDelegate(
      //   routesBuilder: (context) => appRoutes,
      // ),
      // routeInformationParser: RoutemasterParser(),
      home: Navigator(
        pages: [
          MaterialPage(
            key: ValueKey('FarmPage'),
            child: FarmScreen(),
          ),
        ],
        onPopPage: (route, result) {
          if (!route.didPop(result)) {
            return false;
          }
          return true;
        },
      ),
    );
  }
}
