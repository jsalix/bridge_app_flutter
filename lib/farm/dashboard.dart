import 'package:flutter/material.dart';
// import 'package:routemaster/routemaster.dart';

// import '../routes.dart';

class DashboardScreen extends StatelessWidget {
  final Function onTapProducts;

  const DashboardScreen({Key? key, required this.onTapProducts})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
      body: Column(
        children: [
          Card(
            child: InkWell(
              onTap: () => onTapProducts(),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 40,
                  horizontal: 60,
                ),
                child: Row(
                  children: [
                    Text(
                      '4',
                      style: TextStyle(
                        color: Colors.green,
                        fontSize: 32,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        right: 40,
                      ),
                    ),
                    Text(
                      'Products',
                      style: TextStyle(
                        fontSize: 24,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Card(
            child: InkWell(
              onTap: () {},
              child: Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 40,
                  horizontal: 60,
                ),
                child: Row(
                  children: [
                    Text(
                      '0',
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 32,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        right: 40,
                      ),
                    ),
                    Text(
                      'Orders',
                      style: TextStyle(
                        fontSize: 24,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Card(
            child: InkWell(
              onTap: () {},
              child: Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 30,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Forms',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
