class Product {
  final String name;
  final String description;
  final int amount;
  final String unit;

  Product({
    required this.name,
    required this.description,
    required this.amount,
    required this.unit,
  });

  Product.empty()
      : name = '',
        description = '',
        amount = 0,
        unit = '';
}
