import 'package:bridge_app/farm/dashboard.dart';
import 'package:flutter/material.dart';

import 'products/products.dart';

class FarmScreen extends StatefulWidget {
  const FarmScreen({Key? key}) : super(key: key);

  @override
  _FarmScreenState createState() => _FarmScreenState();
}

class _FarmScreenState extends State<FarmScreen> {
  bool showProducts = false;
  bool showOrders = false;
  bool showForms = false;

  @override
  Widget build(BuildContext context) {
    return Navigator(
      pages: [
        MaterialPage(
          key: ValueKey('DashboardPage'),
          child: DashboardScreen(
            onTapProducts: () {
              setState(() {
                showProducts = true;
              });
            },
          ),
        ),
        if (showProducts)
          MaterialPage(
            key: ValueKey('InventoryListPage'),
            child: ProductsScreen(),
          ),
      ],
      onPopPage: (route, result) {
        if (!route.didPop(result)) {
          return false;
        }

        if (showProducts) showProducts = false;
        if (showOrders) showOrders = false;
        if (showForms) showForms = false;

        return true;
      },
    );
  }
}
