import 'package:flutter/material.dart';

import '../shared/models.dart';
import 'inventory_list.dart';
import 'product_detail.dart';

class ProductsScreen extends StatefulWidget {
  const ProductsScreen({Key? key}) : super(key: key);

  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  List<Product> products = [
    Product(
      name: 'Cherry Reds',
      description: 'Small and delicious',
      amount: 5,
      unit: 'lb',
    ),
    Product(
      name: 'Golden Tubes',
      description: 'Fresh batch, just pulled',
      amount: 18,
      unit: 'lb',
    ),
    Product(
      name: 'Big Greens',
      description: 'Our most popular one',
      amount: 0,
      unit: 'lb',
    ),
    Product(
      name: 'Purple Heads',
      description: 'New batch! Try this one out',
      amount: 14,
      unit: 'lb',
    ),
  ];

  bool addingNewProduct = false;
  bool editingProduct = false;
  Product? selectedProduct;

  @override
  Widget build(BuildContext context) {
    return Navigator(
      pages: [
        MaterialPage(
          key: ValueKey('InventoryListPage'),
          child: InventoryListScreen(
            products: products,
            onAddNewProduct: () {
              setState(() {
                addingNewProduct = true;
              });
            },
            onTapProduct: (Product product) {
              setState(() {
                editingProduct = true;
                selectedProduct = product;
              });
            },
          ),
        ),
        if (addingNewProduct)
          MaterialPage(
            key: ValueKey('AddProductPage'),
            child: ProductDetailScreen(
              title: 'Add Product',
              product: Product.empty(),
            ),
          ),
        if (editingProduct)
          MaterialPage(
            key: ValueKey('EditProductPage'),
            child: ProductDetailScreen(
              title: 'Edit Product',
              product: selectedProduct!,
            ),
          )
      ],
      onPopPage: (route, result) {
        if (!route.didPop(result)) {
          return false;
        }

        if (addingNewProduct) addingNewProduct = false;
        if (editingProduct) {
          editingProduct = false;
          selectedProduct = null;
        }

        return true;
      },
    );
  }
}
