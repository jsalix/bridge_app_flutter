import 'package:flutter/material.dart';

import '../shared/models.dart';

class InventoryListScreen extends StatelessWidget {
  final List<Product> products;
  final Function onAddNewProduct;
  final Function onTapProduct;

  InventoryListScreen({
    required this.products,
    required this.onAddNewProduct,
    required this.onTapProduct,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listed Inventory'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView(
        children: [
          for (Product product in this.products) _productListItem(product)
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => onAddNewProduct(),
      ),
    );
  }

  Widget _productListItem(Product product) {
    return ListTile(
      leading: CircleAvatar(
        backgroundImage: NetworkImage('https://picsum.photos/id/305/100'),
      ),
      title: Text(product.name),
      subtitle: Text(product.description),
      trailing: Text(
        product.amount > 0 ? product.amount.toString() + product.unit : 'OoS',
      ),
      onTap: () => onTapProduct(product),
    );
  }
}
